import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { ChatProvider, Message } from '../../providers/chat/chat';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/scan';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  messages: Observable<Message[]>;
  myForm: FormGroup;

  constructor(public navCtrl: NavController,
    public chat: ChatProvider,
    public formBuilder: FormBuilder
  ) {
    this.myForm = this.formBuilder.group({
      formValue: ['', Validators.required]
    });
    this.messages = this.chat.conversation.asObservable()
      .scan((acc, val) =>
        acc.concat(val)
      );
  }

  sendMessage(){
    this.chat.converse(this.myForm.controls['formValue'].value);
    this.myForm.reset();
  }
}
