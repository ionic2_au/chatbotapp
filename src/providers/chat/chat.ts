import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DIALOGFLOW } from '../../app/app.dialogflow.config';
import { ApiAiClient } from 'api-ai-javascript';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// 1. 建立Message類別，定義每一道訊息的欄位
//    訊息欄位：內容 / 傳送者
export class Message {
  constructor(public content: string, public sentBy: string) {}
}

@Injectable()
export class ChatProvider {
  // Dialogflow agent提供的access token
  readonly token = DIALOGFLOW.dialogflow.angularBot;
  // 建立ApiAiClient 自然語言處理client物件
  readonly client = new ApiAiClient({accessToken: this.token});

  // 可供訂閱的對話Observable資料流
  conversation = new BehaviorSubject<Message[]>([]);

  constructor(public http: HttpClient) {
    console.log('Hello ChatProvider Provider');
  }

  converse(msg: string){
    const userMessage = new Message(msg, 'user');
    // console.log('usermessage',userMessage);
    this.update(userMessage);

    return this.client.textRequest(msg).then(res=>{
      const speech = res.result.fulfillment.speech;
      const botMessage = new Message(speech, 'bot');
      this.update(botMessage);
    });
  }

  // Adds message to source
 update(msg: Message) {
   this.conversation.next([msg]);
 }

}
